const Banco = require('./Banco');
class MetodoDeposito {
    
            constructor(empleado) {
                this.nombre=empleado.nombre;
                this.sueldo=empleado.obtenerSalario();
                this.nroCuenta=empleado.nroCuenta; 
                this.bancoDeposito=new Banco;
                this.fecha=new Date();                          
            }
            pagadoPorDeposito(){
                return this.cajaChica=this.cajaChica-this.sueldo;
            }

            generarEstracto() {
                return `Recibo\nFecha: ${this.fecha.getDate() + '-' + (this.fecha.getMonth()+1) + '-' + this.fecha.getFullYear()}\nRecibido: ${this.nombre}\nMonto: ${this.sueldo}`;
            }
            generarMetodoDePago(){
                return this.generarRecibo();
                
            }
            
}
module.exports = MetodoDeposito;