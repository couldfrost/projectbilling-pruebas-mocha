class MetodoCheque {
    
            constructor(empleado,fecha) {
                this.nombre=empleado.nombre;
                this.sueldo=empleado.obtenerSalario();
                this.fecha= new Date(fecha);
                
            }
        
            generarCheque() {
                return `Cheque\nFecha: ${this.fecha.getDate() + '-' + (this.fecha.getMonth()+1) + '-' + this.fecha.getFullYear()}\nPagase: ${this.nombre}\nMonto: ${this.sueldo}`;
            }
            generarMetodoDePago(){
                return this.generarCheque();
                
            }
            
}
module.exports = MetodoCheque;