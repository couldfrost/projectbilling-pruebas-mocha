

class Empleado {

    constructor(nombre, ci, tipo,calcularSalario,calcularFecha, metodoPago, nroDeCuenta,correoElectronico) {
        this.nombre = nombre;
        this.ci = ci;
        this.tipo = tipo;
        this.calcularSalario = calcularSalario;
        this.calcularFecha = calcularFecha;
        this.metodoPago = metodoPago;
        this.nroDeCuenta = nroDeCuenta;
        this.correoElectronico = correoElectronico;
    }

    obtenerNombre() {
        return this.nombre;
    }

    obtenerCi() {
        return this.ci;
    }

    obtenerSalario() {
        return this.calcularSalario.obtenerSalario();
    }

    obtenerFechaPago() {
        return this.calcularFecha.inicioDePago();
    }

}

module.exports = Empleado;