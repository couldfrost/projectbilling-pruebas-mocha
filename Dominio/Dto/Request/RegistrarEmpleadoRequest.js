class RegistrarEmpleadoRequest {
    constructor(ci,nombre,tipo,sindicato) {
        this.ci=ci;
        this.nombre=nombre;
        this.tipo=tipo;
        this.sindicato=sindicato;
    }

}

module.exports = RegistrarEmpleadoRequest;