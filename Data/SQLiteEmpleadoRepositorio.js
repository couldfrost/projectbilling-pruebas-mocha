const sqlite3 = require('sqlite3').verbose();
const Empleado = require('../Dominio/Entities/Empleado/Empleado.js');
//const CalcularSalario = require('./CalcularSalario');

class SQLiteEmpleadoRepositorio {
    constructor() {
        this.db = new sqlite3.Database('./dataBase.db', (err) => {
            if(err) {
                return console.log(err.message);
            }
            this.db.run("CREATE TABLE IF NOT EXISTS empleado(ci INTEGER PRIMARY KEY,nombre TEXT,type TEXT)");
        });
    }

    insertarEmpleado(empleado) {
        this.db.run('INSERT INTO empleado (ci, name, type) VALUES (?,?,?)', empleado);
    }

    leerTodosEmpleados() {
        return new Promise((resolve, reject) => {
            this.db.all("SELECT * FROM empleado", function(err) {
                if(err) {
                    return console.log(err.message);
                }
            }, function(err, empleados) {
                if(err) {
                    reject(err);
                }
                let response = [];
                empleados.forEach(empleado => {
                    //let calcularSalario = new CalcularSalario(empleado.type);
                    let nuevoEmpleado = new Empleado(empleado.name, empleado.ci, empleado.tipo);
                    response.push(nuevoEmpleado);
                });
                resolve(response);
            });
        });
    }

    encontrarEmpleado(employeeId) {
        this.db.all("SELECT * FROM empleado WHERE id=?", employeeId, function(err, rows) {
            rows.forEach(function (row) {
                console.log(row.ci + ": " + row.name + ": " + row.type);
            });
        });
    }

    actualizarEmpledo(employee) {
        this.db.run('UPDATE empleado SET name = ?, type = ? WHERE ci = ?;', [employee[1], employee[2], employee[0]], (err, rows) => {
            if(err) {
                return console.log(err.message);
            }
            console.log(`Row(s) updated: ${this.changes}`);
        });
    }

    eliminarEmpleado(employeeId) {
        this.db.run(`DELETE FROM empleado WHERE ci=?`, employeeId, function(err) {
            if(err) {
                return console.log(err.message);
            }
            console.log(`Row(s) deleted: ${this.changes}`);
        });
    }

    cerrarConexion() {
        db.close((err) => {
            if(err) {
                return console.log('err.message');
            }
        });
    }

}

module.exports = SQLiteEmpleadoRepositorio;
