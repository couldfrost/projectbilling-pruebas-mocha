let expect = require('chai').expect;

import CalculadoraFechaPagoFijo from "../Dominio/Entities/CalculadoraDeFecha/CalculadoraFechaPagoFijo";
import CalcularFechaPagoComision from "../Dominio/Entities/CalculadoraDeFecha/CalculadoraFechaPagoComision";
import CalcularFechaPagoHoras from "../Dominio/Entities/CalculadoraDeFecha/CalculadoraFechaPagoHoras";


describe('CalcularFechaPago', function() {

    it('Deberia retornar que es posible pagar a un empleado por hora en viernes', function() {
        let fecha = new Date("2019-04-05 14:00:00");
        //let empleado = new Empleado('', 1, 'Horas', null, null, null, null, fecha);
        let calculadoraFechaPago = new CalcularFechaPagoHoras(fecha);
        expect(calculadoraFechaPago.inicioDePago()).equal('5-4-2019');
    });

    it('Deberia no retornar que es posible pagar a un empleado por hora en viernes', function() {
        let fecha = new Date("2019-04-06 14:00:00");
        //let empleado = new Empleado('', 1, 'Horas', null, null, null, null, fecha);
        let calculadoraFechaPago = new CalcularFechaPagoHoras(fecha);
        expect(calculadoraFechaPago.inicioDePago()).equal('12-4-2019');
    });

    it('Deberia retornar que es posible pagar a un empleado por comision', function() {
        let fechaInicial = new Date("2019-04-01 08:00:00");
        //let empleado = new Empleado('', 1, 'Comision', null, null, null, null, fechaInicial);
        let calculadoraFechaPago = new CalcularFechaPagoComision(fechaInicial);
        expect(calculadoraFechaPago.inicioDePago()).equal('12-4-2019');
    });

    it('Deberia no retornar que es posible pagar a un empleado por comision', function() {
        let fechaInicial = new Date("2019-04-27 08:00:00");
        //let empleado = new Empleado('', 1, 'Comision', null, null, null, null, fechaInicial);
        let calculadoraFechaPago = new CalcularFechaPagoComision(fechaInicial);
        expect(calculadoraFechaPago.inicioDePago()).equal('10-5-2019');
    });

    it('Deberia retornar que es posible pagar a un empleado fijo', function() {
        let fechaInicial = new Date("2019-04-01 08:00:00");
        //let empleado = new Empleado('', 1, 'Fijo', null, null, null, null, fechaInicial);
        let calculadoraFechaPago = new CalculadoraFechaPagoFijo(fechaInicial);
        expect(calculadoraFechaPago.inicioDePago()).equal('30-4-2019');
    });

    it('Deberia no retornar que es posible pagar a un empleado fijo', function() {
        let fechaInicial = new Date("2019-06-01 08:00:00");
        //let empleado = new Empleado('', 1, 'Fijo', null, null, null, null, fechaInicial);
        let calculadoraFechaPago = new CalculadoraFechaPagoFijo(fechaInicial);
        expect(calculadoraFechaPago.inicioDePago()).equal('28-6-2019');
    });

});