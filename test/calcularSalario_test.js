import SalesTable from "../Dominio/Entities/Tabla/TablaVentas";

let expect = require('chai').expect;
import CalcularSalarioFijo from '../Dominio/Entities/CalculadoraSalario/CalculadoraSalarioFijo.js';
import CalcularSalarioHoras from '../Dominio/Entities/CalculadoraSalario/CalculadoraSalarioHoras.js';
import CalcularSalarioComision from '../Dominio/Entities/CalculadoraSalario/CalculadoraSalarioComision.js';
import ScheduleTable from '../Dominio/Entities/Tabla/TablaHorarios.js';


describe('CalcularSalario', function() {

    it('Deberia retornar el sueldo de un empleado Fijo', function() {
        //let empleado = new Empleado('', 1, 'Fijo', 1000, null, null, null, '2019-04-01');
        let calcularSalario = new CalcularSalarioFijo(1000,'2019-04-01');
        expect(calcularSalario.obtenerSalario()).equal(1000);
    });

    it('Deberia retornar el sueldo de un empleado Fijo', function() {
        //let empleado = new Empleado('', 1, 'Fijo', 500, null, null, null, '2019-04-16');
        let calcularSalario = new CalcularSalarioFijo(500,'2019-04-16');
        expect(calcularSalario.obtenerSalario()).equal(500);
    });

    it('Deberia retornar el sueldo de un empleado Fijo', function() {
        //let empleado = new Empleado('', 1, 'Fijo', 600, null, null, null, '2019-04-11');
        let calcularSalario = new CalcularSalarioFijo(600,'2019-04-11');
        expect(calcularSalario.obtenerSalario()).equal(600);
    });

    it('Deberia retornar el sueldo de un empleado Fijo', function() {
       // let empleado = new Empleado('', 1, 'Fijo', 1000, null, null, null, '2020-04-01');
        let calcularSalario = new CalcularSalarioFijo(1000,'2020-04-01');
        expect(calcularSalario.obtenerSalario()).equal(0);
    });

    it('Deberia retornar el sueldo de un empleado Fijo', function() {
        //let empleado = new Empleado('', 1, 'Fijo', 1000, null, null, null, '2019-03-01');
        let calcularSalario = new CalcularSalarioFijo(1000,'2019-03-01');
        expect(calcularSalario.obtenerSalario()).equal(1000);
    });

    it('Deberia retornar el sueldo de un segundo empleado Fijo', function() {
       // let empleado = new Empleado('', 1, 'Fijo', 2000);
        let calcularSalario = new CalcularSalarioFijo(2000);
        expect(calcularSalario.obtenerSalario()).equal(2000);
    });

    it('Deberia retornar el sueldo de un empleado Horas', function() {
        let schedule = new ScheduleTable([
            ['2019-05-21 14:00:00', '2019-05-21 16:00:00'],
            ['2019-05-21 18:00:00', '2019-05-21 20:00:00']
        ]);
       // let empleado = new Empleado('', 1, 'Horas', 100, schedule);
        let calcularSalario = new CalcularSalarioHoras(100,schedule);
        expect(calcularSalario.obtenerSalario()).equal(400);
    });

    it('Deberia retornar el sueldo de un segundo empleado Horas', function() {
        let schedule = new ScheduleTable([
            ['2019-05-21 14:00:00', '2019-05-21 16:00:00'],
            ['2019-05-21 18:00:00', '2019-05-21 21:00:00']
        ]);
      //  let empleado = new Empleado('', 1, 'Horas', 200, schedule);
        let calcularSalario = new CalcularSalarioHoras(200,schedule);
        expect(calcularSalario.obtenerSalario()).equal(1000);
    });

    it('Deberia retornar el sueldo de un segundo empleado por Horas Extra', function() {
        let schedule = new ScheduleTable([
            ['2019-05-21 10:00:00', '2019-05-21 22:00:00']
        ]);
        //let empleado = new Empleado('', 1, 'Horas', 100, schedule);
        let calcularSalario = new CalcularSalarioHoras(100,schedule);
        expect(calcularSalario.obtenerSalario()).equal(1400);
    });

    it('Deberia retornar el sueldo de un empleado Comision', function() {
        let sales = new SalesTable([
            ['2019-05-21 14:00:00', 2000],
            ['2019-05-21 15:00:00', 4000]
        ]);
        //let empleado = new Empleado('', 1, 'Comision', 1000, null, 0.5, sales);
        let calcularSalario = new CalcularSalarioComision(1000,0.5,sales);
        expect(calcularSalario.obtenerSalario()).equal(4000);
    });

});
