let expect = require('chai').expect;
import CalcularSalarioFijo from '../Dominio/Entities/CalculadoraSalario/CalculadoraSalarioFijo.js';
import CalcularSalarioHoras from '../Dominio/Entities/CalculadoraSalario/CalculadoraSalarioHoras.js';
import CalcularSalarioComision from '../Dominio/Entities/CalculadoraSalario/CalculadoraSalarioComision.js';
import MetodoCheque from "../Dominio/Entities/MetodoDePago/MetodoCheque";
import MetodoDeposito from "../Dominio/Entities/MetodoDePago/MetodoDeposito";
import MetodoRRHH from "../Dominio/Entities/MetodoDePago/MetodoRRHH";
import Empleado from '../Dominio/Entities/Empleado/Empleado.js';

describe('Metodo de pago', function() {
    
        it('Deberia retornar el cheque del empleado', function() {
            let calcularSalario = new CalcularSalarioFijo(2000,'2019-04-01');
            let empleado = new Empleado('Alvaro', 20, 'Fijo',calcularSalario,null,"Cheque");
            let metododepago = new MetodoCheque(empleado,"2019-4-14");
            expect(metododepago.generarMetodoDePago()).equal('Cheque\nFecha: 14-4-2019\nPagase: Alvaro\nMonto: 2000');
        });
        it('Deberia retornar el recibo del empleado', function() {
            let calcularSalario = new CalcularSalarioFijo(2000,'2019-04-01');
            let empleado = new Empleado('Alvaro', 20, 'Fijo',calcularSalario,null,"RRHH");
            let metododepago = new MetodoRRHH(empleado,5000,"2019-5-14");
            expect(metododepago.generarMetodoDePago()).equal('Recibo\nFecha: 14-5-2019\nRecibido: Alvaro\nMonto: 2000');
        });
    });